﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZMovieAssessment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /*
         * In Form 1 wird das Hauptfenster geladen mit den Buttons Einloggen,Neuer Mitspieler und Beenden. In der ComboBox werden alle zum
         * aktuellen Zeitpunkt verfügbaren Mitspieler angezeigt. Der Dateipfad ist in einem String "connectionString" hinterlegt weil
         * TODO zukünftig geplant ist, diesen individuell pro PC zu wählen, um so verschieden Dateipfade zu nutzen. Die Datenbankzugriffe
         * sind alle public und als Attribute gesetzt, da Sie in mehreren Methoden verwendung finden. Der Integer Volume steht in relation 
         * mit einem Ticker und wird pro tick volume++ hochgezählt. Dies ergibt einen kleinen grafischen Effekt im Hauptmenue.
         */
        public string connectionString { get; set; } = "Data Source=C:\\Temp\\MovieDataBase.accdb";
        public OleDbConnection con { get; set; } = null;
        public OleDbDataAdapter da { get; set; } = null;
        public DataTable dt { get; set; } = null;
        public DataView dv { get; set; } = null;
        public DataRowView drv { get; set; } = null;

        private int volume;
        /*
         * Beim Laden von Form1 wird der Ticker gestartet und die User(Mitspieler) in die comboBox geladen. Dabei wird die
         * Methode userListAnzeigen(Parameter) mit der comboBox als Parameter aufgerufen.
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            lblVersion.Text = "Pre-Alpha 2164_T12";

            timerSoundVolume.Start();

            con = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + connectionString);

            try
            {
                con.Open();
                userListAnzeigen(comboUserList);
            }
            catch (Exception eex)
            {
                MessageBox.Show(eex.Message);
                MessageBox.Show("Fehler beim öffnen der Datenbank\n");
            }
            finally
            {
                con.Close();
            }
        }
        /*
         * Ist die comboBox leer oder wurde kein Mitspieler ausgewählt, kommt die entsprechende Meldung.
         * Bei erfolgreichem "Einloggen" wird Form2 geladen das Hauptmenue versteckt (INFO kann bei falscher anwendung zu erheblichen problemen 
         * führen, da das Fenster nie geschlossen wird und so ein weiterer Start des Programms verhindert wird)
         * Der gewählte User(Mitspieler) wird für Form2 gemerkt indem der Inhalt der comboUserList.SelectedItem an das Form2 Attribut globalPlayer
         * übergeben wird. Dieser wird in Form2 aber auch in weitere Forms benötigt. Wenn die Forms2 geschlossen wird, wird anschließend das
         * Hauptmenue wieder sichtbar gemacht.
         */
        private void cmdLogin_Click(object sender, EventArgs e)
        {
            if (comboUserList.SelectedIndex == -1)
            {
                MessageBox.Show("Es ist kein Mitspieler ausgewählt", "Fehlender Benutzer");
                return;
            }

            this.Hide();
            Form2 f2 = new Form2();
            f2.globalPlayer = comboUserList.SelectedItem.ToString();

            if (f2.ShowDialog() == DialogResult.OK)
            {
                this.Show();
            }
            else
            {
                this.Show();
            }
        }
        /*
         * Bei click auf "Neuen User" wird Form3 geladen das Hauptmenue versteckt (INFO kann bei falscher anwendung zu erheblichen problemen 
         * führen, da das Fenster nie geschlossen wird und so ein weiterer Start des Programms verhindert wird)
         * In Forms3 gibt es nur zwei Buttons mit Beenden und Speichern. Klickt man auf Beenden wird Form3 geschlossen und Form1 wieder
         * sichtbar gemacht. Bei klicken des Speichern Buttons wird ein Neuer Nutzer in die Datenbank hinzugefügt. Dabei werden alle nötigen 
         * Datenbankzugriffe geladen. Anschließend wird durch eine simple mechanik kontrolliert ob der NEU eingegebene Benutzer bereits vorhanden
         * ist. Anschleißend wird die userListAnzeigen(Parameter) Methode aufgerufen, um die comboBox bei erfolgreichen implementieren zu 
         * aktualisieren, der con geschlossen und das Hauptfenster wieder sichtbar gemacht.
         */
        private void cmdNewUser_Click(object sender, EventArgs e)
        {
            Form3 f3 = new Form3();

            this.Hide();

            if (f3.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    con.Open();

                    da = new OleDbDataAdapter("SELECT * FROM Benutzer", con);
                    OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
                    OleDbCommand cmd = new OleDbCommand();
                    dt = new DataTable();

                    da.Fill(dt);

                    dv = new DataView(dt);
                    int vergleich = dv.Count;

                    for (int i = 0; i < dv.Count; i++)
                    {
                        // Es wird kontrolliert ob der eingegebene neue Nutzer bereits vorhanden ist. Hierbei wird kontrolliert ob von 
                        // dv[i]["UserName"] in Großbuchstaben gesetzt den gleichen inhalt hat wie das Form3 Attribut input, was den Inhalt
                        // von der in Form3 beheimateten textBox wiedergibt.
                        if (dv[i]["UserName"].ToString().ToUpper() != f3.input.ToUpper())
                        {
                            vergleich--;
                        }
                        // Sind bei der Prüfung keine bereits vorhandene gleichen Einträge zu finden muss der int vergleich auf 0 laufen.
                        // Ist dies geschehen kann der Insert Befehl ausgeführt werden
                        if (vergleich == 0)
                        {
                            cmd.Connection = con;
                            cmd.CommandText = "INSERT INTO Benutzer (UserName) " +
                                              "VALUES('" + f3.input + "')";
                            cmd.ExecuteNonQuery();

                            MessageBox.Show(this, "Mitspieler wurde hinzugefügt");
                        }
                    }
                    // Ist der int vergleich nicht auf 0 gelaufen. Muss ein gleicher Eintrag bestehen.
                    if (vergleich > 0)
                    {
                        MessageBox.Show(this, "Dieser Mitspieler ist bereits vorhanden");
                    }
                }
                catch
                {
                }
                finally
                {
                    userListAnzeigen(comboUserList);

                    con.Close();
                    this.Show();
                }
            }
            else
            {
                this.Show();
            }
        }
        /*
         * Eine kleine Spielerei. Da nicht jeder bzw versehentlich User gelöscht werden sollen. Ist der Löschen Button versteckt.
         * Die checkBox die den Button aktivieren soll ist im Hauptmenue versteckt bzw gut Integriert.
         */
        private void checkLöschen_CheckedChanged(object sender, EventArgs e)
        {
            if (checkLöschen.Checked)
            {
                cmdUserLöschen.Visible = true;
            }
            else
            {
                cmdUserLöschen.Visible = false;
            }
        }
        /*
         * Ist der Löschen Button aktiviert kann jeder ausgewählte Mitspieler, der noch keine Einträge gemacht hat, gelöscht werden.
         */
        private void cmdUserLöschen_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, "Mitspieler können nur gelöscht werden, wenn sie noch keine Einträge getätigt haben.");

            if (comboUserList.SelectedIndex != -1)
            {
                string msg = "Wollen Sie den Spieler " + comboUserList.SelectedItem + " wirklich löschen?";

                string cpt = "Spieler löschen";

                if (MessageBox.Show(this, msg, cpt, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        // TODO Unwiederrufliches Löschen mit zwei DELETE Befehlen die erst die Einträge löschen und danach den User;

                        con.Open();
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "DELETE FROM Benutzer WHERE UserName ='" + comboUserList.SelectedItem + "'";
                        cmd.ExecuteNonQuery();
                        MessageBox.Show(this, "Spieler wurde gelöscht");
                        userListAnzeigen(comboUserList);
                    }
                    catch
                    {
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            else
                MessageBox.Show(this, "Kein Mitspieler zum Löschen ausgewählt.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        /*
         * Beendet die Anwendung
         */
        private void cmdBeenden_Click(object sender, EventArgs e)
        {
            Close();
        }
        /*
         * Füllt die ComboBox mit den bestehenden User.
         * ComboBox ist als Parameter gesetzt weil für spätere Anwendungen geplant ist, die User auch in Anderen
         * ComboBoxen anzeigen zu lassen.
         */
        public void userListAnzeigen(ComboBox combo)
        {
            combo.Items.Clear();

            da = new OleDbDataAdapter("SELECT * FROM Benutzer", con);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            dt = new DataTable();
            da.Fill(dt);

            dv = new DataView(dt);
            for (int i = 0; i < dv.Count; i++)
            {
                combo.Items.Add(dv[i]["UserName"]);
            }
        }
        /*
         * Der rekursive Grafische ablauf der Volumeanzeige
         */
        private void timerSoundVolume_Tick(object sender, EventArgs e)
        {
            volume++;

            switch (volume)
            {
                case 7:
                    pn1.BackColor = Color.Red;
                    break;
                case 14:
                    checkLöschen.BackColor = Color.Red;
                    break;
                case 21:
                    pn2.BackColor = Color.Orange;
                    break;
                case 28:
                    pn3.BackColor = Color.Yellow;
                    break;
                case 35:
                    pn4.BackColor = Color.Yellow;
                    break;
                case 42:
                    pn5.BackColor = Color.YellowGreen;
                    break;
                case 49:
                    pn6.BackColor = Color.GreenYellow;
                    break;
                case 56:
                    pn7.BackColor = Color.Green;
                    break;
                case 70:
                    checkLöschen.BackColor = Color.Black;
                    pn1.BackColor = Color.Gray;
                    pn2.BackColor = Color.Gray;
                    pn3.BackColor = Color.Gray;
                    pn4.BackColor = Color.Gray;
                    pn5.BackColor = Color.Gray;
                    pn6.BackColor = Color.Gray;
                    pn7.BackColor = Color.Gray;
                    volume = 0;

                    break;
            }
        }
    }
}

