﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZMovieAssessment
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }
        /*
        * Die aus anderne Forms übergebenen Attribute
        */
        public string globalPlayer { get; set; }
        private void cmdOK_Click(object sender, EventArgs e)
        {
           
            if (globalPlayer == txtName.Text)
            {
                if (MessageBox.Show(this, "Möchtest du die Änderungen speichern?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.Cancel;
                }
            }
            else
            {
                MessageBox.Show("Du hast diese Bewertung nicht ursprünglich abgegeben.\nKeine Änderung möglich!");
                DialogResult = DialogResult.Cancel;
            }
        }
        private void cmdCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        /* Übernimmt die aus der vorangegangenen Form übergenen Parameter und setzt sie in die dafür bereitgestellten Felder
         * zum editieren. txtName und txtTitel sind readonly denn diese können nicht Editiert werden. Nur Jahr und Note können abgeändert
         * werden
         */
        public void filmEditieren(DataRowView drv)
        {
            if (drv.Row.RowState == DataRowState.Detached) // detached = abgetrennt
            {
                txtName.Text = "Name";
                txtTitel.Text = "Titel";
                txtYear.Text = "Year";
                txtGrade.Text = "Note";
            }
            else //wenn ein vorhandener Datensatz geändert wird
            {
                txtName.Text = drv["UserName"].ToString();
                txtTitel.Text = drv["MovieName"].ToString();
                txtYear.Text = drv["Datum"].ToString();
                txtGrade.Text = drv["Bewertung"].ToString();

            }
            if (this.ShowDialog() == DialogResult.OK) // "OK"
            {
                drv.BeginEdit();
                //drv["MovieName"] = txtTitel.Text;
                //drv["UserName"] = txtName.Text;
                drv["Datum"] = txtYear.Text;
                drv["Bewertung"] = txtGrade.Text;

                drv.EndEdit();
            }
            else 
                drv.CancelEdit();
        }
        /*
         * Es sind nur nummerische Eingaben erlaubt. Andere Eingaben werden ignoriert
         */
        private void txtGrade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        /*
         * Es sind nur nummerische Eingaben erlaubt. Andere Eingaben werden ignoriert
         */
        private void txtYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
