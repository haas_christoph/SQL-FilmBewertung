﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZMovieAssessment
{
    public partial class Form2 : Form
    {
        
        public Form2()
        {
            InitializeComponent();
        }
        /*
        * Die aus anderne Forms übergebenen Attribute
        */
        private int ticks;
        public string globalPlayer;
        public string[] globalPlayers;
        /*
         * Grafische Spielerei. Nach einer gewissen Zeit soll die bei cmdBewertungAbgeben_Click aktivierten Buttons wieder
         * verschwinden
         */
        private void timer1_Tick(object sender, EventArgs e)
        {
            ticks++;

            if (ticks == 75)
            {
                cmdDirekt.Visible = false;
                cmdMitFormular.Visible = false;
                timer1.Stop();
                ticks = 0;
            }
        }
        /*
        * Grafische Spielerei. Aktiviert zwei nicht sichtbare Buttons für zwei eingabemöglichkeiten.
        */
        private void cmdBewertungAbgeben_Click(object sender, EventArgs e)
        { 
            timer1.Start();

            cmdDirekt.Visible = true;
            cmdMitFormular.Visible = true;
        }
        /*
        * Gibt das signal zurück DialogResult.Cancel was in anderen Forms verwertet und dementsprechend bearbeitet wird.
        */
        private void cmdZurück_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        /*
        * Öffnet Form4 was aktuell nur eine Konzeptmechanik präsentiert.
        */
        private void cmdMitFormular_Click(object sender, EventArgs e)
        {
            Form4 f4 = new Form4();

            if (f4.ShowDialog() == DialogResult.OK)
            {
                //TODO Hier sollen die Eingaben aus dem Formular verarbeitet werden und die eingegebenen Daten in die Datenbank landen.
            }
        }
        /*
        * Beim klicken wird For52 geladen und Form2 versteckt (INFO kann bei falscher anwendung zu erheblichen problemen 
        * führen, da das Fenster nie geschlossen wird und so ein weiterer Start des Programms verhindert wird)
        * Das aus Form 1 übergebene Attribut wird an Form5 weitergegeben hier an den gleichnamigen globalPlayer. Zudem wird ein HARDCODIERTER
        * Parameter an Form5 übergeben hier an den int switcher. Dies hat den Grund das Form5 in aktuell zwei mechaniken Funktioniert und durch
        * die übergabe des HARDCODIERTEN Parameters weiß welche Mechanik aktiviert werden soll.
        * 
        */
        private void cmdDirekt_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form5 f5 = new Form5();
           
            f5.globalPlayer = globalPlayer;
            f5.switcher = 1;

            if (f5.ShowDialog() == DialogResult.OK)
            {
                this.Show();
            }
            else
            {
                this.Show();
            }
        }
        /*
       * Beim klicken wird For5 geladen und Form2 versteckt (INFO kann bei falscher anwendung zu erheblichen problemen 
       * führen, da das Fenster nie geschlossen wird und so ein weiterer Start des Programms verhindert wird)
       * Das aus Form 1 übergebene Attribut wird an Form5 weitergegeben hier an den gleichnamigen globalPlayer. Zudem wird ein HARDCODIERTER
       * Parameter an Form5 übergeben hier an den int switcher. Dies hat den Grund das Form5 in aktuell zwei mechaniken Funktioniert und durch
       * die übergabe des HARDCODIERTEN Parameters weiß welche Mechanik aktiviert werden soll.
       * 
       */
        private void cmdWertungenAnzeigen_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form5 f5 = new Form5();
            f5.globalPlayer = globalPlayer;
            f5.switcher = 2;

            if (f5.ShowDialog() == DialogResult.OK)
            {
                this.Show();
            }
            else
            {
                this.Show();
            }
        }
    }
}
