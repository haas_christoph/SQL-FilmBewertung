﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZMovieAssessment
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        /*
         * String input gibt den wert der txtNeuerUser zurück. Somit ist er in anderen Formen mit f2.input aufrufbar.
         */
        public string input
        {
            get { return txtNeuerUser.Text; }
        }
        /*
         * Gibt das signal zurück DialogResult.OK was in anderen Forms verwertet und dementsprechend bearbeitet wird.
         */
        private void cmdSpeichern_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
        /*
        * Gibt das signal zurück DialogResult.Cancel was in anderen Forms verwertet und dementsprechend bearbeitet wird.
        */
        private void cmdZurueck_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
