﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZMovieAssessment
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }
        /*
        * Die aus anderne Forms übergebenen Attribute
        */
        public string globalPlayer { get; set; }
        public int switcher { get; set; }
        /*
        * Die Datenbankzugriffe werden in Form 5 vermehrt benötigt.
        * TODO wenn sinnvoll diese auch aus Form1 beziehen.
        */
        public string connectionString { get; set; } = "Data Source=C:\\Temp\\MovieDataBase.accdb";

        public OleDbDataAdapter da { get; set; } = new OleDbDataAdapter();
        public OleDbCommand cmd { get; set; } = new OleDbCommand();
        public DataTable dt { get; set; } = new DataTable();
        public DataView dv { get; set; } = new DataView();
        public DataRowView drv { get; set; } = null;
        public OleDbConnection con { get; set; } = null;
        public OleDbCommandBuilder cb { get; set; } = null;
        /*
         * Form5 wird in aktuell zwei Mechaniken unterschieden, hierfür wird eine SwitchCase Anweisung verwendet
         * Um Form5 unterschiedlich nutzen zu können, müssen die gegebenen txt,comboBoxen ec. unterschiedliche angewand und geladen werden
         * 
         * CASE 1
         * Sollen Bewertungen abgegeben werden aber auch bereits vorhandene abgeändert werden können.
         * Hier werden alle bereits vorhandenen Filme in die comboBox geladen.
         * 
         * CASE 2
         * Kann man sich alle Bewertungen anschauen und sie gegebenenfalls nochmals ändern.
         * Hier werden alle bestehenden Genres in der comboBox sowie alle bewerteten Filme mit dazugehörigen Usern und Note ind die
         * entsprechenden Felder geladen. Dazu werden die Methoden genreList(), userList() und  alleBewertungen(mit individuellem Befehlsparameter)
         * verwendet. Es werden alle Tabelleneigenschaften gesetz (Font, Breite, Visible...)
         */
        private void Form5_Load(object sender, EventArgs e)
        {
            con = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + connectionString);

            dgvMovieList.Rows.Clear();
            dgvMovieList.ColumnHeadersVisible = false;
            dgvMovieList.RowHeadersVisible = false;

            switch (switcher)
            {
                case 1:

                    cmdBewertungAbgeben.Visible = true;

                    movieSuchen(switcher);
                    //userExtrapolieren();
                    MessageBox.Show(this, "Für eine Filmbewertung, wähle bitte einen Film aus der Liste aus oder suche Ihn im Titelfeld\n " +
                    "Wird der gewünschte Film nicht gefunden wird ein neuer Eintrag generiert", "Kleine Einweisung");

                    foreach (DataGridViewTextBoxColumn c in dgvMovieList.Columns)
                    {
                        c.Width = 250;
                        c.DefaultCellStyle.Font = new Font("Grunge Handwriting", 16);
                        c.DefaultCellStyle.ForeColor = Color.White;
                    }
                    dgvMovieList.Sort(dgvMovieList.Columns["MovieName"], ListSortDirection.Ascending);
                    break;

                case 2:
                    comboUser.Visible = true;
                    lblUser.Visible = true;

                    genreList();
                    comboGenre.SelectedIndex = 0;
                    userList();
                    comboUser.SelectedIndex = 0;

                    string befehl = "SELECT * FROM Bewertungen WHERE MovieName LIKE '%" + txtTitel.Text + "%'";
                    alleBewertungen(befehl);

                    dgvMovieList.Columns[2].Width = 65;
                    dgvMovieList.Columns[3].Width = 125;
                    dgvMovieList.Columns[4].Width = 35;


                    for (int i = 0; i < dgvMovieList.ColumnCount; i++)
                    {
                        dgvMovieList.Columns[i].HeaderCell.Style.Font = new Font("Grunge Handwriting", 22);
                        dgvMovieList.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    }
                    foreach (DataGridViewTextBoxColumn c in dgvMovieList.Columns)
                    {
                        c.DefaultCellStyle.Font = new Font("Grunge Handwriting", 16);
                        c.DefaultCellStyle.ForeColor = Color.White;
                    }

                    break;
            }

            dgvMovieList.RowsDefaultCellStyle.BackColor = Color.FromArgb(87, 110, 118);
            dgvMovieList.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(65, 137, 161);

            dgvMovieList.EnableHeadersVisualStyles = false;
            dgvMovieList.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dgvMovieList.ColumnHeadersDefaultCellStyle.BackColor = Color.Black;
        }
        /*
         * Die Anwendung wird geschlossen 
         */
        private void button2_Click(object sender, EventArgs e)
        {
            comboUser.Visible = false;
            lblUser.Visible = false;
            cmdBewertungAbgeben.Visible = false;
            DialogResult = DialogResult.Cancel;
        }
        /*
         * movieSuchen Methode wird beim drücken der Taste ENTER ausgeführt
         */
        private void txtTitel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                movieSuchen(switcher);
            }
        }
        /*
         * Je nach ausgewählter Mechanik werden,durch eingabe in das Textfeld, entweder alle Filme oder alle Filme 
         * und dessen Bewertungen sowie User in die ComboBox geladen.
         */
        private void txtTitel_TextChanged(object sender, EventArgs e)
        {
            switch (switcher)
            {
                case 1:
                    movieSuchen(switcher);
                    break;

                case 2:
                    string befehl = "SELECT * FROM Bewertungen WHERE MovieName LIKE '%" + txtTitel.Text + "%'";
                    alleBewertungen(befehl);

                    break;
            }
        }
        /*
         * Damit das TextChange Event keine falschen Parameter übergeben bekommt, werden beim klicken der Jahresanzeige der aktuelle
         * String in einen String.Empty gewandelt 
         */
        private void txtYear_Click(object sender, EventArgs e)
        {
            txtYear.Text = "";
        }
        /*
         * Es sind nur nummerische Eingaben erlaubt. Andere Eingaben werden ignoriert
         */
        private void txtYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        /*
         * Es werden alle Einträge Angezeigt, die im txtYear angegeben wird
         */
        private void txtYear_TextChanged(object sender, EventArgs e)
        {
            switch (switcher)
            {
                case 1:

                    break;

                case 2:
                    if (txtYear.Text == string.Empty)
                    {
                        break;
                    }
                    //TODO evt. Umbau in eine ListBox

                    //else if (Convert.ToInt32(txtYear.Text) <= 2015 || Convert.ToInt32(txtYear.Text) >= 2250)
                    //{
                    //    txtYear.Text = "2021";
                    //    string befehl = "SELECT Datum, UserName,MovieName, Bewertung FROM Bewertungen WHERE Datum LIKE '%" + txtYear.Text + "%'";
                    //    alleBewertungen(befehl);
                    //    dgvMovieList.Columns[0].Visible = false;
                    //    break;
                    //}
                    else
                    {
                        string befehl = "SELECT * FROM Bewertungen WHERE Datum LIKE '%" + txtYear.Text + "%'";
                        alleBewertungen(befehl);
                        break;
                    }
            }
        }
        /*
         * Es werden alle Einträge Angezeigt, die im txtGrade angegeben wird. Da Eingaben über 10 und unter -5(evt auch nur 0)
         * nicht erlaubt sind werden diese bei einer Eingabe automatisch in 10 oder -5 korrigiert. die Werte bzw. korrigierten Werte
         * werden in den string befehl übergeben, der für die anschließenden Methodenaufrufe benötigt wird.
         * Die Suchergebnisse werden mittels der Methoden alleBewertungen() angezeigt.
         */
        private void txtGrade_TextChanged(object sender, EventArgs e)
        {
            string befehl = "SELECT * FROM Bewertungen WHERE Bewertung = " + txtGrade.Text + "";

            switch (switcher)
            {
                case 1:

                    break;

                case 2:
                    if (txtGrade.Text == string.Empty)
                    {
                        befehl = "SELECT * FROM Bewertungen WHERE Bewertung LIKE '%%' ";
                        alleBewertungen(befehl);

                        break;
                    }
                    else if (Convert.ToInt32(txtGrade.Text) >= 11)
                    {
                        txtGrade.Text = string.Empty;
                        txtGrade.Text = "10";

                        alleBewertungen(befehl);

                        break;
                    }
                    else if (Convert.ToInt32(txtGrade.Text) <= -5)
                    {
                        txtGrade.Text = string.Empty;
                        txtGrade.Text = "-5";

                        alleBewertungen(befehl);

                        break;
                    }
                    else
                    {
                        alleBewertungen(befehl);

                        break;
                    }
            }
        }
        /*
         * Es sind nur nummerische Eingaben erlaubt. Andere Eingaben werden ignoriert
         */
        private void txtGrade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        /*
         * Es werden alle User angezeigt die vorab in die comboBox geladen wurde. Ist keiner ausgewählt werden die Bewertungen aller angezeigt
         * Ist ein User ausgewählt werden nur dessen Bewertungen mittels der Methoder alleBewertungen angezeigt.
         */
        private void comboUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (switcher)
            {
                case 1:

                    break;

                case 2:
                    if (comboUser.Text == "-All-")
                    {
                        string befehl = "SELECT * FROM Bewertungen WHERE UserName LIKE '%%'";
                        alleBewertungen(befehl);

                        break;
                    }
                    else
                    {
                        string befehl = "SELECT * FROM Bewertungen WHERE UserName = '" + comboUser.Text + "'";
                        alleBewertungen(befehl);

                        break;
                    }
            }
        }
        /*
         * Es werden alle Genres angezeigt die vorab in die comboBox geladen wurde. Ist keiner ausgewählt werden die Bewertungen aller 
         * Genren angezeigt. Ist ein Genre ausgewählt werden nur dessen Bewertungen mittels der Methoder alleBewertungen angezeigt.
         */
        private void comboGenre_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (switcher)
            {
                case 1:

                    break;

                case 2:
                    if (comboGenre.Text == "-All-")
                    {
                        string befehl = "SELECT * FROM Bewertungen WHERE UserName LIKE '%%'";
                        alleBewertungen(befehl);

                        break;
                    }
                    else
                    {
                        string befehl = "SELECT Bewertungen.Datum, Bewertungen.UserName,Bewertungen.MovieName, Bewertungen.Bewertung, Movies.Genre FROM Bewertungen, Movies WHERE Bewertungen.MovieName = Movies.MovieName AND Movies.Genre LIKE '%" + comboGenre.Text + "%'";

                        alleBewertungenFuerGenre(befehl);

                        break;
                    }
            }
        }
        /*
         * Beim klicken des Buttons BewertungAbgeben wird erst überprüft ob alle relevanten Felder gefüllt sind. Ist dies nicht der Fall
         * kommt eine MessageBox. Ist dies der Fall wird geprüft ob bereits eine Bewertung des gleichen Users mit den genannten Filmtitel
         * besteht. Ist dies so kann die Bewertung geändert werden. Trifft dies nicht zu wird eine neue Bewertung dieses User generiert.
         * Ist die ein Film der noch von keinen User bewertet wurde, wird dieser erstmals in die Datenbank aufgenommen.
         */
        private void cmdBewertungAbgeben_Click(object sender, EventArgs e)
        {
            //TODO Vorababfrage ob der gewählte Filmtitel bereits besteht... dann sind untere Abfragen -DEAKTIVIERT-

            if (txtGrade.Text == string.Empty)
            {
                MessageBox.Show(this, "Bitte eine Filmbewertung eingeben");
                return;
            }

            else if (txtYear.Text == string.Empty || txtYear.Text == "Year")
            {
                MessageBox.Show(this, "Bitte das Jahr eingeben in dem du den Film angeschaut hast");
                return;
            }
            else if (txtTitel.Text == string.Empty)
            {
                MessageBox.Show(this, "Bitte einen Filmtitel eingeben");
                return;
            }
            else if (comboGenre.Text == string.Empty)
            {
                MessageBox.Show(this, "Bitte ein Filmgenre angeben");
                return;
            }
            if (dgvMovieList.Rows.Count > 1)
            {
                string befehl = "INSERT INTO Bewertungen (Datum, UserName, MovieName, Bewertung) VALUES(" + txtYear.Text +
                     ",'" + globalPlayer + "','" + dgvMovieList.CurrentCell.Value + "'," + txtGrade.Text + ")";

                //string update = "UPDATE Movies SET Genre = '" + comboGenre.SelectedItem + "' WHERE UserName = '" + dgvMovieList.CurrentCell.Value + "'";
                //MessageBox.Show(update);
                bewerten(befehl);
                //bewertungAender(update);
                movieSuchen(switcher);
            }
            else
            {
                if (DialogResult.Yes == MessageBox.Show(this, "Der Film: " + txtTitel.Text + " hat noch keine Bewertungen\n" +
                    "Soll dieser in die Movieliste aufgenommen und mit deiner Note bewertet werden?", "Neuer Filmeintrag Eintrag", MessageBoxButtons.YesNo))
                {
                    neuerFilm();
                    movieSuchen(switcher);
                    string befehl = "INSERT INTO Bewertungen (Datum, UserName, MovieName, Bewertung) VALUES(" + txtYear.Text +
                         ",'" + globalPlayer + "','" + dgvMovieList.CurrentCell.Value +
                        "'," + txtGrade.Text + ")";
                    bewerten(befehl);
                    movieSuchen(switcher);
                }
            }
        }
        /*
         * Sucht einen Film mit den Parametern aus der TextBox und schreibt das Gefundene in das DataGridView
         */
        private void movieSuchen(int zahl)
        {
            try
            {
                con.Open();

                switch (zahl)
                {
                    case 1:
                        da = new OleDbDataAdapter("SELECT MovieName FROM Movies WHERE MovieName LIKE '%" + txtTitel.Text + "%'", con);
                        break;
                    case 2:
                        da = new OleDbDataAdapter("SELECT * FROM Bewertungen WHERE MovieName LIKE '%" + txtTitel.Text + "%'", con);
                        break;
                }

                cb = new OleDbCommandBuilder(da);
                dt = new DataTable();
                da.Fill(dt);

                dv = new DataView(dt);

                dgvMovieList.DataSource = dt;
            }
            catch
            {
                MessageBox.Show("Fehler beim öffnen der Datenbank\n");
            }
            finally
            {
                con.Close();
            }
        }
        /*
         * Sucht alle Filme mittels übergebenen BefehlsParameter und schreibt das Gefundene in das DataGridView
         */
        private void alleBewertungen(string befehl)
        {
            try
            {
                con.Open();

                da = new OleDbDataAdapter(befehl, con);
                cb = new OleDbCommandBuilder(da);
                dt = new DataTable();
                da.Fill(dt);

                dv = new DataView(dt);

                dgvMovieList.DataSource = dt;
                //dgvMovieList.Sort(dgvMovieList.Columns["MovieName"], ListSortDirection.Ascending);

                dgvMovieList.Columns[0].Visible = false;
                dgvMovieList.Columns[1].Visible = false;

            }
            catch
            {
                MessageBox.Show("Fehler beim öffnen der Datenbank\n");
            }
            finally
            {
                con.Close();
            }
        }
        /*
         * Da der SQL Befehl für die Suche nach Genres aus zwei Tabellen besteht. Gibt es eine gesonderte Methode extra nur für die
         * Genresuche
         */
        private void alleBewertungenFuerGenre(string befehl)
        {
            try
            {
                con.Open();

                da = new OleDbDataAdapter(befehl, con);
                cb = new OleDbCommandBuilder(da);
                dt = new DataTable();
                da.Fill(dt);

                dv = new DataView(dt);

                dgvMovieList.DataSource = dt;
                //dgvMovieList.Sort(dgvMovieList.Columns["MovieName"], ListSortDirection.Ascending);

                dgvMovieList.Columns[0].Visible = false;
                dgvMovieList.Columns[4].Visible = false;

            }
            catch
            {
                MessageBox.Show("Fehler beim öffnen der Datenbank\n");
            }
            finally
            {
                con.Close();

            }
        }
        /*
         * Ein neuer Film wird in die Datenbank geschrieben
         */
        private void neuerFilm()
        {
            string befehl = "INSERT INTO Movies (MovieName, Genre) VALUES ('" + txtTitel.Text + "','" +
                comboGenre.SelectedItem + "')";

            OleDbConnection con = new OleDbConnection();

            OleDbCommand cmd = new OleDbCommand();

            con.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" + connectionString;

            cmd.Connection = con;

            cmd.CommandText = befehl;
            try
            {
                con.Open();

                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                con.Close();
            }
        }
        /*
         * Ein neuer Film wird in Bewertet. Es wird vorab kontrolliert ob es bereits eine Bewertung dieses Filmes, des ausgewählten Users gibt
         * Ist dies der Fall wird die BewertungAendern Methode ausgführt die die Bewertung in den Punkten Note und Datum abändert.
         * Ist es eine neue Bewertung wird diese in die Datenbank geschrieben. Vorab gibt es noch abfragen an den User ob die 
         * Eingaben stimmen
         */
        private void bewerten(string befehl)
        {
            OleDbConnection con = new OleDbConnection();

            OleDbCommand cmd = new OleDbCommand();

            con.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" + connectionString;

            cmd.Connection = con;

            cmd.CommandText = befehl;
            try
            {
                //TODO mögliche optimierung in da = new OleDbDataAdapter(...
                con.Open();

                da = new OleDbDataAdapter("SELECT UserName,MovieName, Bewertung FROM Bewertungen WHERE MovieName = '" + txtTitel.Text + "'", con);
                cb = new OleDbCommandBuilder(da);
                dt = new DataTable();
                da.Fill(dt);
                dv = new DataView(dt);

                for (int i = 0; i < dv.Count; i++)
                {
                    if (dv[i]["UserName"].ToString().ToUpper() == globalPlayer.ToUpper() &&
                        dv[i]["MovieName"].ToString().ToUpper() == dgvMovieList.CurrentCell.Value.ToString().ToUpper())
                    {
                        if (DialogResult.Yes == MessageBox.Show("Für diesen Film hast du bereits eine Bewertung abgegeben, möchtest du diese änder", "", MessageBoxButtons.YesNo))
                        {
                            string update = "UPDATE Bewertungen SET Datum = " + txtYear.Text + ", Bewertung = " + txtGrade.Text + " WHERE UserName = '" + globalPlayer + "' AND MovieName = '" + dgvMovieList.CurrentCell.Value + "'";
                            bewertungAender(update);
                            return;
                        }

                        return;
                    }
                }
                if (DialogResult.Yes == MessageBox.Show(globalPlayer + " möchtest de den Film\n\n" + txtTitel.Text + "\n\nmit " +
                                       "" + txtGrade.Text + " von 10 bewertet", "Deine Filmbewertung"
                                       , MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                {
                    try
                    {
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Film wurde Bewertet");
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                con.Close();
            }
        }
        /*
         * Ändert die Bewertung der bereits vorhandenen Bewertung
         */
        private void bewertungAender(string updatebefehl)
        {
            OleDbConnection con = new OleDbConnection();

            OleDbCommand cmd = new OleDbCommand();

            con.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" + connectionString;

            cmd.Connection = con;

            cmd.CommandText = updatebefehl;

            con.Open();

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Bewertung wurde geändert");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                con.Close();
            }
        }
         /* Wie in Form1
          * Füllt die ComboBox mit den bestehenden User.
          * TODO aus Form1
          * ComboBox ist als Parameter gesetzt weil für spätere Anwendungen geplant ist, die User auch in Anderen
          * ComboBoxen anzeigen zu lassen.
          * Für zb. ComboBox aus Form5
          */
        private void userList()
        {
            comboUser.Items.Clear();

            da = new OleDbDataAdapter("SELECT * FROM Benutzer", con);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            dt = new DataTable();
            da.Fill(dt);

            dv = new DataView(dt);

            comboUser.Items.Add("-All-");

            for (int i = 0; i < dv.Count; i++)
            {
                comboUser.Items.Add(dv[i]["UserName"]);
            }
        }
         /*
          * Zeigt alle Genre aus der Datenbank an
          */
        private void genreList()
        {
            comboGenre.Items.Clear();

            da = new OleDbDataAdapter("SELECT Genre FROM Movies", con);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            dt = new DataTable();
            da.Fill(dt);

            dv = new DataView(dt);

            comboGenre.Items.Add("-All-");

            for (int i = 0; i < dv.Count; i++)
            {
                int count = 0;
                if (dv[i]["Genre"].ToString() != String.Empty)
                {
                    for (int j = 0; j < comboGenre.Items.Count; j++)
                    {
                        if (dv[i]["Genre"].ToString() == comboGenre.Items[j].ToString())
                        {
                            count++;
                        }
                    }
                    if (count == 0)
                    {
                        comboGenre.Items.Add(dv[i]["Genre"]);
                    }
                }
            }
        }
        /*
         * Doppelklick ermöglicht es in
         * CASE1
         * Den FilmNamen in der Textbox anzeigen zu lassen
         * 
         * CASE2
         * Eine Änderung der Einträge zu veranlassen. Öffnet dabei Form6 und die Methode aus Form6 filmEditieren mit übergebenen Parameter
         * drv
         */
        private void dgvMovieList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            switch (switcher)
            {
                case 1:
                    txtTitel.Text = dgvMovieList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    break;
                case 2:
                    drv = dv[dgvMovieList.CurrentRow.Index];

                    Form6 f6 = new Form6();
                    f6.globalPlayer = globalPlayer;
                    f6.filmEditieren(drv);
                    f6.Dispose();
                    cmdAenderungSpeichern.Visible = true;
                    break;
            }
        }
        /*
         * Speichert die Änderungen aus Form6
         * TODO evt schon in dgvMovieList_CellMouseDoubleClick implementieren
         */
        private void cmdAenderungSpeichern_Click(object sender, EventArgs e)
        {
            DataTable dt1 = dt.GetChanges();

            string zeile = String.Empty;

            if (!(dt1 == null))
            {
                try
                {
                    con.Open();

                    int m = da.Update(dt1);
                    string s = "Anzahl der Änderungen: " + m.ToString();
                    MessageBox.Show(s, "Speichern war erfolgreich!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    dt.AcceptChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Speichern fehlgeschlagen!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    dt.RejectChanges();
                }
                finally
                {
                    con.Close();
                    cmdAenderungSpeichern.Visible = false;
                }
            }
        }
    }
}

